import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool showWidget = false;
  List<Widget> widgets = List<Widget>.generate(
    30,
    (index) => Container(
      width: 30,
      height: 30,
      decoration: index % 2 != 0
          ? BoxDecoration(color: Colors.amber, border: Border.all())
          : BoxDecoration(
              color: Colors.green,
            ),
    ),
  );

  Widget buildWidget(BuildContext context) {
    if (showWidget) {
      return ListView.builder(
        padding: EdgeInsets.all(0),
        itemCount: widgets.length,
        itemBuilder: (context, index) {
          return widgets[index];
        },
      );
    } else {
      return GridView.builder(
        padding: EdgeInsets.all(0),
        itemCount: widgets.length,
        gridDelegate:
            SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 200),
        itemBuilder: (context, index) {
          return widgets[index];
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.1,
            width: double.infinity,
            child: RaisedButton(
                color: Colors.grey,
                child: Text('Переключатель'),
                onPressed: () {
                  setState(() {
                    showWidget = !showWidget;
                  });
                }),
          ),
          Container(
              height: MediaQuery.of(context).size.height * 0.9,
              child: buildWidget(context)),
        ],
      ),
    );
  }
}
